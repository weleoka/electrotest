#!/bin/sh
##!/usr/bin/fish

# --- ELECTROTEST test-program ---
# Test-programet testar för:
# Felaktiga inmatade resistansvärden, tex resistanser på Gf ohm 
# Orimliga värden, tex -3 antal motstånd 
# Extrema värden på antal och resistanser 
# Värden som kan medföra division med 0 

# Input to electrotest is as follows:
# Voltage
# Serial/Paralell circuit [s|p]
# Component count
# component 1
# component 2
# ... 


###
# Make a list of all resistor values increased by a certain increment.
get_component_list () {
	CC=$1	# Component Count
	RI=$2	# Resistance increment
	CR=$3	# Current resistance
	i=0

	while [ "$i" -lt "$CC" ]; do
		if [ "$i" -eq "0" ]; then
			VAL="$CR"
		else
			VAL="$VAL\n$CR"
		fi
		CR=$((CR + RI))
		i=$((i+1))
	done

	echo "$VAL"	# Shell knows only returned integers or echoed strings.
	return 0
}



###
# Display current test values.
print_test_params () {
	i=0
	for value in $8; do
		if [ $i -eq "0" ]; then
			STRING="$value"
		else
			STRING="$STRING, $value"
		fi
		
		i=$((i + 1))

		if [ $i -ge "10" ]; then
			STRING="$STRING, ..., .."
			break
		fi
	done
	echo "\n*** Test $1: $2 ***
	Voltage: $3
	Circuit type: $4
	Component count: $5
	Resistance of first component: $6
	Resistance increment: $7
	Values: $STRING"
	return 0
}



###
# Get user yes no input. 
yes_no_input () {
	while [ true ]; do
		echo "Sorry, try again"
		read trythis

		if [ $trythis = "y" ]; then
			return 0
		elif [ $trythis = "n" ]; then
			return 1
		fi
	done
}



###
# The executing of the program with test parameters. 
# Note the indentation is wrong but that's to make the here document happy.
execute_test () {
	echo "\t\tNow running test $1... \n"
	
./electrotest <<!FUNKY!
$2
$3
$4
$5
!FUNKY!

	echo "\n   Completed test $1. Press enter to continue."
	read xyzish
	return 0
}



echo "\t- - - ELECTROTEST test-program - - -\n\t\t\tby weleoka.\n"



# Test number 1. Faulty resistance values.
TEST_NUMBER=1
TEST_TYPE="Felaktiga resistansvärden."

VOLTAGE=12	# The voltage of the circuit.
CIRCUIT_TYPE="s"	# Valid types s/p for series and paralell.
COMPONENT_COUNT=3	# The number of components in the circuit.
RESISTANCE_INCREMENT="na"	# Increase in resistance for each new component.
CURRENT_RESISTANCE="na"	# The resistance of the first component.
VALUES="gf zm qr" #$(get_component_list $COMPONENT_COUNT $RESISTANCE_INCREMENT $CURRENT_RESISTANCE)

print_test_params $TEST_NUMBER "$TEST_TYPE" $VOLTAGE $CIRCUIT_TYPE $COMPONENT_COUNT $CURRENT_RESISTANCE $RESISTANCE_INCREMENT "$VALUES"
execute_test $TEST_NUMBER $VOLTAGE $CIRCUIT_TYPE $COMPONENT_COUNT "$VALUES"



# Test number 2. Impossible values.
TEST_NUMBER=2
TEST_TYPE="Orimliga värden."

VOLTAGE=12	# The voltage of the circuit.
CIRCUIT_TYPE="s"	# Valid types s/p for series and paralell.
COMPONENT_COUNT=-3	# The number of components in the circuit.
RESISTANCE_INCREMENT=2	# Increase in resistance for each new component.
CURRENT_RESISTANCE=-100	# The resistance of the first component.
VALUES=""	#$(get_component_list $COMPONENT_COUNT $RESISTANCE_INCREMENT $CURRENT_RESISTANCE)

print_test_params $TEST_NUMBER "$TEST_TYPE" $VOLTAGE $CIRCUIT_TYPE $COMPONENT_COUNT $CURRENT_RESISTANCE $RESISTANCE_INCREMENT "$VALUES"
execute_test $TEST_NUMBER $VOLTAGE $CIRCUIT_TYPE $COMPONENT_COUNT "$VALUES"




# Test number 3. Extreme values.
TEST_NUMBER=3
TEST_TYPE="Extrema värden."

VOLTAGE=12	# The voltage of the circuit.
CIRCUIT_TYPE="s"	# Valid types s/p for series and paralell.
COMPONENT_COUNT=100	# The number of components in the circuit.
RESISTANCE_INCREMENT=2	# Increase in resistance for each new component.
CURRENT_RESISTANCE=9999999999	# The resistance of the first component.
VALUES=$(get_component_list $COMPONENT_COUNT $RESISTANCE_INCREMENT $CURRENT_RESISTANCE)

print_test_params $TEST_NUMBER "$TEST_TYPE" $VOLTAGE $CIRCUIT_TYPE $COMPONENT_COUNT $CURRENT_RESISTANCE $RESISTANCE_INCREMENT "$VALUES"
execute_test $TEST_NUMBER $VOLTAGE $CIRCUIT_TYPE $COMPONENT_COUNT "$VALUES"




# Test number 4. Division by 0.
TEST_NUMBER=4
TEST_TYPE="Division med 0."

VOLTAGE=12	# The voltage of the circuit.
CIRCUIT_TYPE="s"	# Valid types s/p for series and paralell.
COMPONENT_COUNT=3	# The number of components in the circuit.
RESISTANCE_INCREMENT="na"	# Increase in resistance for each new component.
CURRENT_RESISTANCE="na"	# The resistance of the first component.
VALUES="0 0 0"	# $(get_component_list $COMPONENT_COUNT $RESISTANCE_INCREMENT $CURRENT_RESISTANCE)

print_test_params $TEST_NUMBER "$TEST_TYPE" $VOLTAGE $CIRCUIT_TYPE $COMPONENT_COUNT $CURRENT_RESISTANCE $RESISTANCE_INCREMENT "$VALUES"
execute_test $TEST_NUMBER $VOLTAGE $CIRCUIT_TYPE $COMPONENT_COUNT "$VALUES"




# Test number 5. Error in circuit option.
TEST_NUMBER=4
TEST_TYPE="Felaktigt kopplingsalternativ."

VOLTAGE=12	# The voltage of the circuit.
CIRCUIT_TYPE="z"	# Valid types s/p for series and paralell.
COMPONENT_COUNT=1	# The number of components in the circuit.
RESISTANCE_INCREMENT="2"	# Increase in resistance for each new component.
CURRENT_RESISTANCE="2"	# The resistance of the first component.
VALUES=$(get_component_list $COMPONENT_COUNT $RESISTANCE_INCREMENT $CURRENT_RESISTANCE)

print_test_params $TEST_NUMBER "$TEST_TYPE" $VOLTAGE $CIRCUIT_TYPE $COMPONENT_COUNT $CURRENT_RESISTANCE $RESISTANCE_INCREMENT "$VALUES"
execute_test $TEST_NUMBER $VOLTAGE $CIRCUIT_TYPE $COMPONENT_COUNT "$VALUES"




exit 0



# Quoting (") does matter to preserve multi-line values.
# In addition to the backticks, you can use $(), which I find easier to read, and allows for nesting.
# OUTPUT="$(ls -1)"
# echo "${OUTPUT}"

# The colon command is a null command. It’s occasionally useful to simplify the logic of conditions, being
# an alias for true. Since it’s built-in, : runs faster than true, though its output is also much less readable.
# You may see it used as a condition for while loops; while : implements an infinite loop in place of the
# more common while true.

# The export command makes the variable named as its parameter available in subshells. By default,
# variables created in a shell are not available in further (sub)shells invoked from that shell. The export
# command creates an environment variable from its parameter that can be seen by other scripts and pro-
# grams invoked from the current program.


# A problem with variables: an empty variable STUFF="" in an if statement will not be valid code for comparison.
# For example if [ $STUFF="abc" ]; is actually writing if [ ="abc" ]; which is invalid.
# The variable has to be inside double quotes. if [ "$STUFF"="abc" ]; which then writes if [ ""="abc" ];

# Single quotes in bash will suppress special meaning of all meta characters. Therefore meta characters will be read literally.
# Double quotes in bash will suppress special meaning of all meta characters except for "$", "\" and "`". 

# Useful code snippets:
# if [ $trythis = "y" ] || [ $trythis = "n" ]; then


# Arithmetic comparisons
# -lt 	<
# -gt 	>
# -le 	<=
# -ge 	>=
# -eq 	==
# -ne 	!=

# String comparisons
# = 	equal
# != 	not equal
# -n s1 	string s1 is not empty
# -z s1 	string s1 is empty

# File testing
# -b filename 	Block special file
# -c filename 	Special character file
# -d directoryname 	Check for directory existence
# -e filename 	Check for file existence
# -f filename 	Check for regular file existence not a directory
# -G filename 	Check if file exists and is owned by effective group ID.
# -g filename 	true if file exists and is set-group-id.
# -k filename 	Sticky bit
# -L filename 	Symbolic link
# -O filename 	True if file exists and is owned by the effective user id.
# -r filename 	Check if file is a readable
# -S filename 	Check if file is socket
# -s filename 	Check if file is nonzero size
# -u filename 	Check if file set-ser-id bit is set
# -w filename 	Check if file is writable
# -x filename 	Check if file is executable


# Shell function, write its name followed by empty parentheses and enclose the statements in braces:
# function_name () {
#	arg1=$1	# first parameter passed
#	arg2=$2	# second parameter passed
# 	statements...
# }
# Call the statment as such: function_name "$arg1 $arg2"
# Note that you can declare local variables within shell functions by using the local keyword. The variable is
# then only in scope within the function. Otherwise, the function can access the other shell variables that are
# essentially global in scope. If a local variable has the same name as a global variable, it overlays that variable,
# but only within the function. 

# The OR list construct executes a series of commands until one succeeds, then stops:
# statement1 || statement2 || statement3 || ...

# The AND list construct executes a series of commands, 
# executing the next command only if all the previous commands have succeeded. 
# statement1 && statement2 && statement3 && ...
# Starting at the left, each statement is executed; if it returns true, the next statement to the right is executed.
# This continues until a statement returns false, after which no more statements in the list are executed. 
# The AND list as a whole succeeds if all commands are executed successfully, but it fails otherwise.

# To use multiple statements in a place where only one is allowed, such as in an AND or OR
# list, do so by enclosing them in braces {} to make a statement block. For example:
# get_confirm && {
# 	grep -v "$cdcatnum" $tracks_file > $temp_file
# 	cat $temp_file > $tracks_file
# 	echo add_record_tracks
# }


# -a makes read command to read into an array
# read -a colours

# -n makes echo command not print newline char at end.
# 	If you want the echo command to delete the trailing new line, the most portable
# 	option is to use the printf command (see the printf section later in this chapter),
# 	rather than the echo command. Some shells use echo –e, but that’s not supported on
#	all systems. bash allows echo -n to suppress the new line, so if you are confident
# 	your script needs to work only on bash, we suggest using that syntax.


# The dot (.) command executes the command in the current shell:
# . ./shell_script
# Normally, when a script executes an external command or script, a new environment (a subshell) is cre-
# ated, the command is executed in the new environment, and the environment is then discarded apart
# from the exit code that is returned to the parent shell. However, the external source and the dot com-
# mand (two more synonyms) run the commands listed in a script in the same shell that called the script.
# Because, by default, a new environment is created when a shell script is executed, any changes to environ-
# ment variables that the script makes are lost. The dot command, on the other hand, allows the executed
# script to change the current environment. This is often useful when you use a script as a wrapper to set up
# your environment for the later execution of some other command. For example, when you’re working on
# several different projects at the same time, you may find you need to invoke commands with different
# parameters, perhaps to invoke an older version of the compiler for maintaining an old program.
# In shell scripts, the dot command works a little like the #include directive in C or C++. Though it doesn’t
# literally include the script, it does execute the command
