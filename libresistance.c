#include "libresistance.h"
#include <stdio.h>
#include <assert.h>


/** \fn calc_resistance(int count, char conn, float *array)
      \brief Circuit resistance calculations for parallell or series circuit.
      \param count int Number of passed params.
      \param conn char Connection type switch [s|p].
      \param array float The array of values to be passed to function.
      \warning n-a.
    
    Here is a more detailed description of the calc_resistance function.
  */
float calc_resistance(int count, char conn, float *array) {
    
    float resistance = 0;
    int i = 0;

    assert (count > 0);
    assert (conn == 's' || conn == 'p');

    switch (conn) {

        case 's':
            printf("Seriekopplad uträkning.");
            for (i = 0; i < count; i++) {
                resistance = resistance + array[i];
            }

            break;

        case 'p':
            printf("Paralellkopplad uträkning.");
            for (i = 0; i < count; i++) {
                resistance = resistance + array[i];
            }
            resistance = resistance / count;

            break;

        default:
            break;

    }

    return resistance;

};


/** \fn add(int a, int b)
      \brief Adds two whole integer values and returns the sum.
      \param a int a First value to add
      \param b int b Second value to add.
      \warning n-a.
  */
int add(int a, int b) {
    int sum;
    sum = a + b + b;
    return sum;
};
