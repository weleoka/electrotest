# Makefile Electrotest linUM_06_projekt by group 113

# The make commands are: 
# all - for compiling a locally installed version of electrotest
# install - to compile and globally install electrotest
# uninstall - to remove electrotest
# tests - to compile libraries and test programs
# lib - to compile libraries only.


# Which compiler
CC = gcc

# Where to install
INSTDIR = /usr/local/bin
LIBINSTDIR = /usr/lib

# Local Libraries
MYLIB = ./lib/

# Where are include files kept
INCLUDE = ./include/

# All the source files which electrotest depends on.
FILES = main.c libresistance.so libpower.so libcomponent.so

# Options for development (optimization level 0, debug, position independent and all warnings)
CFLAGS = -O0 -g -fPIC -Wall -I$(INCLUDE)
# Options for release (Optimization level 2 and ansi??)
# CFLAGS = -O2 –ansi


all: electrotest

electrotest: $(FILES)
	$(CC) $(CFLAGS) main.c -L$(MYLIB) -lpower -lresistance -lcomponent -o electrotest -Wl,-rpath,$(MYLIB)



## LIBRARIES
lib: libresistance.so libcomponent.so libpower.so

# Developers: add dependencies for the other library tests when they are ready.
tests:  libresistance_test libpower_test libcomponent_test

## DOCUMENTATION
documentation:
	doxygen Doxyfile; \
	cd latex; \
	make; \
	cp refman.pdf ../Documentation.pdf;

### LIBRESISTANCE
libresistance_test: libresistance_test.c libresistance.c libresistance.so
	$(CC) $(CFLAGS) libresistance_test.c -L$(MYLIB) -lresistance -o libresistance_test -Wl,-rpath,$(MYLIB)

libresistance.so: libresistance.c
	$(CC) $(CFLAGS) -c libresistance.c -o libresistance.o;
	$(CC) -shared $(CFLAGS) -o $(MYLIB)libresistance.so libresistance.o


### LIBPOWER
libpower_test: libpower_test.c calc_power_r.c calc_power_i.c libpower.so
	$(CC) $(CFLAGS) libpower_test.c -L$(MYLIB) -lpower -o libpower_test -Wl,-rpath,$(MYLIB)

libpower.so:  calc_power_r.c calc_power_i.c
	$(CC) $(CFLAGS) -c calc_power_r.c -o calc_power_r.o;
	$(CC) $(CFLAGS) -c calc_power_i.c -o calc_power_i.o;
	$(CC) -shared $(CFLAGS) -o $(MYLIB)libpower.so calc_power_r.o calc_power_i.o


### LIBCOMPONENT
libcomponent_test: libcomponent_test.c libcomponent.c calc_component.c libcomponent.so
	$(CC) $(CFLAGS) libcomponent_test.c -L$(MYLIB) -lcomponent -o libcomponent_test -Wl,-rpath,$(MYLIB)

libcomponent.so: libcomponent.c 
	$(CC) $(CFLAGS) -c libcomponent.c -o libcomponent.o;
	$(CC) -shared $(CFLAGS) -o $(MYLIB)libcomponent.so libcomponent.o



## Archiving - creating archives of several .c or .o files (not needed for a small project like this)
libcomponent.a:
	ar rc libcomponent.a libcomponent.c calc_component.c

libpower.a:
	ar rc libpower.a libpower.c calc_power_r.c calc_power_i.c

libresistance.a:
	ar rc libresistance.a libresistance.c



### ### ###
u12: objects
	# cd libs; \
	ar cr liblinum.a libcomponent.o libresistance.o calc_power_r.o calc_power_i.o; \
	cp liblinum.a /usr/local/lib; \
	mkdir /usr/local/include/linum; \
	cp libcomponent.h libpower.h libresistance.h /usr/local/include/linum; \
	# cd ..;
# rm /usr/lib/liblinum.a
# rm -R /usr/include/linum
# Bibliotek under /usr/lib (usr/local/lib) Headers: /usr/include (/usr/local/include).

objects:
	$(CC) $(CFLAGS) -c libcomponent.c -o libcomponent.o; \
	$(CC) $(CFLAGS) -c libresistance.c -o libresistance.o; \
	$(CC) $(CFLAGS) -c calc_power_r.c -o calc_power_r.o; \
	$(CC) $(CFLAGS) -c calc_power_i.c -o calc_power_i.o;
### ### ###


## Clean
c: 		clean
claen: 		clean
calean: 	clean
calaen: 	clean
clean:
	@echo "\nRensar upp..."
	@-rm -f electrotest \
	*o \
	*a \
	*so \
	*_test \
	$(MYLIB)*.so;

	@- rm -r -f ./html \
	./latex \
	Documentation.pdf;

	@echo "\nKlart! :)\n"



## Install - use with sudo
install: electrotest
	@if [ -d $(LIBINSTDIR) ];\
		then \
		cp $(MYLIB)*.so $(LIBINSTDIR);\
		chmod 0755 $(LIBINSTDIR)/libpower.so;\
		chmod 0755 $(LIBINSTDIR)/libresistance.so;\
		chmod 0755 $(LIBINSTDIR)/libcomponent.so;\
		ldconfig;\
		echo "Installerade bibliotek i $(LIBINSTDIR)";\
	fi;
	@if [ -d $(INSTDIR) ]; \
		then \
		$(CC) -L$(LIBINSTDIR) -Wall -o electrotest main.c -lcomponent -lresistance -lpower;\
		cp electrotest $(INSTDIR);\
		chmod a+x $(INSTDIR)/electrotest;\
		chmod og-w $(INSTDIR)/electrotest;\
		echo "Installerade electrotest i $(INSTDIR)";\
	else \
		echo "Sorry, $(INSTDIR) finns inte";\
	fi;

	@make clean;


## Uninstall - use with sudo
uninstall:
	@if [ -d $(INSTDIR) ]; \
		then \
		rm $(INSTDIR)/electrotest;\
		echo "Avinstallerade huvudprogramet.";\
	else \
		echo "Error: $(INSTDIR) är inte en katalog.";\
	fi;
	@if [ -d $(LIBINSTDIR) ];\
		then \
		rm $(LIBINSTDIR)/libpower.so;\
		rm $(LIBINSTDIR)/libresistance.so;\
		rm $(LIBINSTDIR)/libcomponent.so;\
		ldconfig;\
		echo "Electrotest bibliotek avinstallerades.";\
	fi;


# GCC options and examples:
# Options starting with -g, -f, -m, -O, -W, or --param are automatically passed on sub-processes invoked by gcc.
# Use the -c option to gcc to tell it just to create an object file (an .o file) rather than an executable:
# To create a .so (shared object) file use the -shared flag to gcc.
# -L. piece tells gcc to look in the current directory in addition to the other library directories for finding libmylib.a.
# To link with a library not in the standard (usr/lib) path: -L/home/myUserName/lib -lmylib
# The compiler option -lNAME will attempt to link object files with a library file ‘libNAME.a’ i
# -I adds include directory of header files. Using -I flag .h files can be in another directory.
# -Wall enables all warnings
# -fPIC enables position independent code, useful for shared libraries.
# -Wl used to send aditional comma sepperated parameters to linker.
# -rpath - Run Time Search Path for finding dependencies.
# -g debug option enabled.
# -O[num] specify optimisation level.
# -static tells gcc to not use dynamic/shared libraries and use static libraries first.
# -export-dynamic is passed to the linker, which creates an executable file that also exports its external symbols as a shared library. This allows modules, which are dynamically loaded as shared libraries, to reference functions from common.c that are linked statically into the server executable.

# The default GNU loader, ld.so, looks for libraries in the following order:
#   It looks in the DT_RPATH section of the executable, unless there is a DT_RUNPATH section.
#   It looks in LD_LIBRARY_PATH. This is skipped if the executable is setuid/setgid for security reasons.
#   It looks in the DT_RUNPATH section of the executable unless the setuid/setgid bits are set (for security reasons).
#   It looks in the cache file /etc/ld/so/cache (disabled with the ‘-z nodeflib’ linker option).
#   It looks in the default directories /lib then /usr/lib (disabled with the ‘-z nodeflib’ linker option).

# If you ever happen to want to link against installed libraries in a given directory, LIBDIR, you must either use libtool, and specify the full pathname of the library, or use the `-LLIBDIR’ flag during linking and do at least one of the following:
# - add LIBDIR to the `LD_LIBRARY_PATH’ environment variable during execution
# - add LIBDIR to the `LD_RUN_PATH’ environment variable during linking
# - use the `-Wl,--rpath -Wl,LIBDIR’ linker flag
# - have your system administrator add LIBDIR to `/etc/ld.so.conf’



# http://www.cprogramming.com/tutorial/shared-libraries-linux-gcc.html


# Special macros and options for makefiles:
# $? List of prerequisites (files the target depends on) changed more recently than the current target
# $@ Name of the current target
# $< Name of the current dependency
# $* Name of the current dependency, without any suffix
# $^ gives you all dependencies, regardless of whether they are more recent than the target. Removes duplicates.
# $+ is like $^, but it keeps duplicates and gives you the entire list of dependencies in the order they appear. 
# - tells make to ignore any errors. For example, if you wanted to make a directory but wished to
# ignore any errors, perhaps because the directory might already exist, you just precede mkdir
# with a minus sign.
# @ tells make not to print the command to standard output before executing it. This character is
# handy if you want to use echo to display some instructions.
# OBJ = $(SRC:.c=.o) is saying: .c should be replaced with .o. 
# $*. For instance, type make <name of .c file> to build an executable file with the given name:
# % appears in the dependencies list, it replaces the same string of text throughout the command in makefile target
# %:
#         gcc -o $* $*.c



# If a user is allowed to read from a directory, the user is allowed to see the list of files that are present in that directory. If a user is allowed to write to a directory, the user is allowed to add or remove files from the directory. Note that a user may remove files from a directory if she is allowed to write to the directory, even if she does not have permission to modify the file she is removing. If a user is allowed to execute a directory, the user is allowed to enter that directory and access the files therein.Without execute access to a directory, a user is not allowed to access the files in that directory independent of the permissions on the files themselves.


# Programs that authenticate users when they log in take advantage of the capability to change user IDs as well.These login programs run as root .When the user enters a username and password, the login program verifies the username and password in the system password database.Then the login program changes both the effective user ID and the real ID to be that of the user. Finally, the login program calls exec to start the user’s shell, leaving the user running a shell whose effective user ID and real user ID are that of the user.
# The kernel will let a process running with an effective user ID of 0 change its user IDs as it sees fit. Any other process, however, can do only one of the following things:
# Set its effective user ID to be the same as its real user ID
# Set its real user ID to be the same as its effective user ID
# Swap the two user IDs

# SATA-I 1.5Gbps = 150MB/s
# SATA-II 3Gbps = 300MB/s
# SATA-III 6Gbps = 600MB/s