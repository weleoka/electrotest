Changelog
=========

Changelog electrotest


Version 0.0.3
-------------
*Release 2016-01-04*
- Development third release.
- Documentation overhaul for doxygen standard.


Version 0.0.2
-------------
*Release 2015-10-28*
- Development second release.
- main.c and Makefile created.


Version 0.0.1
-------------
*Release 2015-10-18*
- Development first version release.
- Shared object source code.
